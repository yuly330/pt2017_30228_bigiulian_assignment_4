/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bankapp;
import com.mycompany.bankapp.Account;
import java.io.Serializable;

/**
 *
 * @author Iulian
 */
 public class Customer implements Serializable {
     private final String firstName;
     private final String lastName;
     private final String cnp;
     private final Account account;
     
     Customer(String firstName, String lastName, String cnp, Account account){
         this.firstName=firstName;
         this.lastName=lastName;
         this.cnp=cnp;
         this.account=account;
         
     }
     @Override
     public String toString(){
         return "\nCustomer Information\n" +
                 "First Name: " +getFirstName() + "\n" +
                 "Last Name: " + getLastName() + "\n" +
                 "CNP: " + getCnp() + "\n" +
                 account;
     }
    public String basicInfo(){
        return "Account Number " + account.getAccountNumber()+ " - Name: " + getFirstName() + " " + getLastName();
    }
    Account getAccount(){
        return account;
    }
    public String getFirstName(){
        return firstName;
    }
    public String getLastName(){
        return lastName;
    }
    public String getCnp(){
        return cnp;
    }
}
