/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bankapp;

/**
 *
 * @author Iulian
 */
class InsufficientFundsException extends Exception {

    public InsufficientFundsException() {
        super("Nu ai destule fonduri pentru a face tranzactia");
    }
    
}
