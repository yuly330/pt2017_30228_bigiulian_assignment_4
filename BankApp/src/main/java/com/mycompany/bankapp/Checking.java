/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bankapp;

import com.mycompany.bankapp.Account;

/**
 *
 * @author Iulian
 */
public class Checking extends Account{
  Checking(int accountNumber, double initialDeposit){
      super(accountNumber);
      this.setBalance(initialDeposit);
  }
    
  @Override
  public AccountType getAccountType(){
      return AccountType.Depozit;
  }
}
