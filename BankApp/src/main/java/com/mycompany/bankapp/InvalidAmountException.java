/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bankapp;

/**
 *
 * @author Iulian
 */
public class InvalidAmountException extends Exception {

    public InvalidAmountException() {
        super("Nu ai destule fonduri pentru a tranzactiona!");
    }
    
}
