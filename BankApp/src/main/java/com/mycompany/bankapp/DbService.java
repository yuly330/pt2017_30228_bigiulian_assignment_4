
package com.mycompany.bankapp;


import com.mycompany.bankapp.Account;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Iulian
 */
public class DbService {
    String url = "jdbc:mysql://localhost:3306/bankdb";
    String user = "bank";
    String password="#Bastan2009";
    
    private Connection connect () {
        Connection connection;
        try {
            connection = DriverManager.getConnection(url, user, password);
        } catch (SQLException ex) {
            connection=null;
        }
        return connection;
    }
    
   int AddAccount (String firstName, String lastName, String cnp, AccountType accountType, double balance){
       int userId = -1;
       int accountId = -1;
       Connection connection = connect();
        try {
            connection.setAutoCommit(false);
            //adaug un utilizator
            String addUserSql="insert into users(FirstName, LastName, CNP) values(?,?,?)";
            PreparedStatement addUser = connection.prepareStatement(addUserSql, Statement.RETURN_GENERATED_KEYS);
            addUser.setString(1, firstName);
            addUser.setString(2, lastName);
            addUser.setString(3, cnp);
            addUser.executeUpdate();
            ResultSet addUserResults = addUser.getGeneratedKeys();
            if (addUserResults.next()){
                userId=addUserResults.getInt(1);
                
            }
            //Adaug un cont
            String addAccountSql ="insert into accounts(Type, Balance) values (?,?)";
            PreparedStatement addAccount = connection.prepareStatement(addAccountSql, Statement.RETURN_GENERATED_KEYS);
            addAccount.setString(1, accountType.name());
            addAccount.setDouble(2, balance);
            addAccount.executeUpdate();
            ResultSet addAccountResults = addAccount.getGeneratedKeys();
            if (addAccountResults.next()){
                accountId=addAccountResults.getInt(1);
            }
            
            //creez o legatura dintre utilizator si cont
            if(userId > 0 && accountId > 0)
            {
                String linkAccountSql = "insert into mappings (UserId, AccountId) values (?,?)";
                PreparedStatement linkAccount = connection.prepareStatement(linkAccountSql);
                linkAccount.setInt(1, userId);
                linkAccount.setInt(2, accountId);
                linkAccount.executeUpdate();
                connection.commit();
            }else {
                connection.rollback();
            }
            connection.close();
            
        } catch (SQLException ex) {
            System.err.println("a survenit o eroare" + ex.getMessage());
        }
        return accountId;
   }
   
 Customer GetAccount(int accountId) {
        Customer customer = null;
        Connection connection = connect();
        try {
            try (PreparedStatement findUser = connection.prepareStatement(
                    "SELECT FirstName,LastName,CNP,Type,Balance "
                    + "FROM Users a JOIN Mappings b on a.ID = b.UserId "
                    + "JOIN Accounts c on b.AccountId = c.ID "
                    + "WHERE c.ID = ?")) {
                findUser.setInt(1, accountId);
                ResultSet findUserResults = findUser.executeQuery();
                if (findUserResults.next()) {
                    String firstName = findUserResults.getString("FirstName");
                    String lastName = findUserResults.getString("LastName");
                    String cnp = findUserResults.getString("CNP");
                    String accountType = findUserResults.getString("Type");
                    Double balance = findUserResults.getDouble("Balance");
                    Account account;
                    if (accountType.equals(AccountType.Depozit.name())) {
                        account = new Checking(accountId, balance);
                    } else {
                        account = new Savings(accountId, balance);
                    }
                    customer = new Customer(firstName, lastName, cnp, account);
                }
            }
        } catch (SQLException ex) {
            System.err.println("An error has occured." + ex.getMessage());
        }
        return customer;
}
          
   boolean UpdateAccount(int accountId, Double newBalance){
        boolean success = false;
        Connection connection = connect();
        try {
            try (PreparedStatement updateBalance = connection.prepareStatement(
                    "UPDATE Accounts SET Balance = ? WHERE ID = ?")) {
                updateBalance.setDouble(1, newBalance);
                updateBalance.setInt(2, accountId);
                updateBalance.executeUpdate();
            }
            success = true;
        } catch (SQLException ex) {
            System.err.println("Eroare" + ex.getMessage());
        }
        return success;
}
   
  boolean DeleteAccount(int accountId) {
        boolean success = false;
        Connection connection = connect();
        try {
            try (PreparedStatement deleteRecords = connection.prepareStatement(
                    "DELETE Users,Accounts FROM Users "
                    + "JOIN Mappings on Users.ID = Mappings.UserId "
                    + "JOIN Accounts on Accounts.ID = Mappings.AccountId "
                    + "WHERE Accounts.ID = ?")) {
                deleteRecords.setInt(1, accountId);
                deleteRecords.executeUpdate();
            }
            success = true;
        } catch (SQLException ex) {
            System.err.println("Eroare" + ex.getMessage());
        }
        return success;
}
     ArrayList<Customer> GetAllAccounts() {
        ArrayList<Customer> customers = new ArrayList<Customer>();
        Connection connection = connect();
        try {
            try (PreparedStatement findUser = connection.prepareStatement(
                    "SELECT AccountId,FirstName,LastName,CNP,Type,Balance "
                    + "FROM Users a JOIN Mappings b on a.ID = b.UserId "
                    + "JOIN Accounts c on b.AccountId = c.ID")) {
                ResultSet findUserResults = findUser.executeQuery();
                while (findUserResults.next()) {
                    String firstName = findUserResults.getString("FirstName");
                    String lastName = findUserResults.getString("LastName");
                    String cnp = findUserResults.getString("CNP");
                    String accountType = findUserResults.getString("Type");
                    Double balance = findUserResults.getDouble("Balance");
                    int accountId = findUserResults.getInt("AccountId");
                    Account account;
                    if (accountType.equals(AccountType.Depozit.name())) {
                        account = new Checking(accountId, balance);
                    } else {
                        account = new Savings(accountId, balance);
                    }
                    Customer customer = new Customer(firstName, lastName, cnp, account);
                    customers.add(customer);
                }
            }
        } catch (SQLException ex) {
            System.err.println("Eroare" + ex.getMessage());
        }
        return customers;
    }
}